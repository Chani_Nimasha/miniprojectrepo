package com.example.exam;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class studentInfo {
    private final SimpleStringProperty fnCol;
    private final SimpleStringProperty lnCol;
    private final SimpleStringProperty userCol;
    private final SimpleStringProperty emailCol;
    private final SimpleStringProperty mobCol;
    private final SimpleStringProperty resCol;

    public studentInfo(String frname, String lname, String username, String email, String mobile, String result1) {
        this.fnCol = new SimpleStringProperty(frname);
        this.lnCol = new SimpleStringProperty(lname);
        this.userCol = new SimpleStringProperty(username);
        this.emailCol = new SimpleStringProperty(email);
        this.mobCol = new SimpleStringProperty(mobile);
        this.resCol = new SimpleStringProperty(result1);
    }

    public String getFnCol() {
        return fnCol.get();
    }

    public SimpleStringProperty fnColProperty() {
        return fnCol;
    }

    public void setFnCol(String fnCol) {
        this.fnCol.set(fnCol);
    }

    public String getLnCol() {
        return lnCol.get();
    }

    public SimpleStringProperty lnColProperty() {
        return lnCol;
    }

    public void setLnCol(String lnCol) {
        this.lnCol.set(lnCol);
    }

    public String getUserCol() {
        return userCol.get();
    }

    public SimpleStringProperty userColProperty() {
        return userCol;
    }

    public void setUserCol(String userCol) {
        this.userCol.set(userCol);
    }

    public String getEmailCol() {
        return emailCol.get();
    }

    public SimpleStringProperty emailColProperty() {
        return emailCol;
    }

    public void setEmailCol(String emailCol) {
        this.emailCol.set(emailCol);
    }

    public String getMobCol() {
        return mobCol.get();
    }

    public SimpleStringProperty mobColProperty() {
        return mobCol;
    }

    public void setMobCol(String mobCol) {
        this.mobCol.set(mobCol);
    }

    public String getResCol() {
        return resCol.get();
    }

    public SimpleStringProperty resColProperty() {
        return resCol;
    }

    public void setResCol(String resCol) {
        this.resCol.set(resCol);
    }
}
