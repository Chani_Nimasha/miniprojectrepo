package com.example.exam;

import eu.hansolo.tilesfx.colors.ColorSkin;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;

import javax.swing.*;
import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.util.*;
import java.util.Timer;
import java.util.concurrent.TimeUnit;

public class StudentDashboard {
    public Button op1;
    public Label question;
    public Button op4;
    public Button op3;
    public Button op2;
    public ProgressIndicator progress;
    public Label result;
    public Button next;
    public Tab quiztab;
    public Button cont;

    public Button submit;
    public Label countdown;
    public Tab res;
    public double Result1;
    public int Y = 0;
    private long min, sec, hr, totalSec = 0; //250 4 min 10 sec
    private Timer timer;

    ArrayList<String> questions = new ArrayList<>();
    ArrayList<String> op1s = new ArrayList<>();
    ArrayList<String> op2s = new ArrayList<>();
    ArrayList<String> op3s = new ArrayList<>();
    ArrayList<String> op4s = new ArrayList<>();
    ArrayList<String> answers = new ArrayList<>();


    private void checkvalid(Button b) {
        String answer = answers.get(Y);
        String value = b.getText();


        if (Objects.equals(value, answer)) {
            b.setStyle("-fx-background-color: #00FF00; ");
            b.setTextFill(ColorSkin.RED);
            Result1 += 1.0;
            progress.setProgress(Result1 / questions.size());
            result.setText("Your Result is " + Result1);


        } else {
            b.setStyle("-fx-background-color: #FF0000; ");
            Result1 -= 1.0;
            progress.setProgress(Result1 / questions.size());
            result.setText("Your Result is " + Result1);


        }
        if (Result1 < 0) {
            Result1 = 0;
            progress.setProgress(Result1 / questions.size());
            result.setText("Your Result is " + Result1);
        }

        setVisible(b);


    }

    private void setVisible(Button button) {
        if (op1.equals(button)) {
            op2.setVisible(false);
            op3.setVisible(false);
            op4.setVisible(false);
        }
        if (op2.equals(button)) {
            op1.setVisible(false);
            op3.setVisible(false);
            op4.setVisible(false);
        }
        if (op3.equals(button)) {
            op2.setVisible(false);
            op1.setVisible(false);
            op4.setVisible(false);
        }
        if (op4.equals(button)) {
            op2.setVisible(false);
            op3.setVisible(false);
            op1.setVisible(false);
        }
        if (Y == questions.size() - 1) {
            cont.setVisible(false);
        }
        if (Y == questions.size() - 1) {
            cont.setVisible(false);
            submit.setVisible(true);
        }

        cont.setVisible(true);


    }

    public void po1(ActionEvent actionEvent) {
        checkvalid(op1);
    }

    public void po2(ActionEvent actionEvent) {
        checkvalid(op2);


    }

    public void po3(ActionEvent actionEvent) {
        checkvalid(op3);


    }

    public void po4(ActionEvent actionEvent) {
        checkvalid(op4);

    }


    public void quizop(Event event) {


        start();
        setTimer();


    }

    private String format(long value) {
        if (value < 10) {
            return 0 + "" + value;
        }

        return value + "";
    }


    public void convertTime() {

        min = TimeUnit.SECONDS.toMinutes(totalSec);
        sec = totalSec - (min * 60);
        hr = TimeUnit.MINUTES.toHours(min);
        min = min - (hr * 60);
        countdown.setText(format(hr) + ":" + format(min) + ":" + format(sec));

        totalSec--;
    }

    private void setTimer() {
        totalSec = 10 * 20 * questions.size() + 1;
        Timer timer = new Timer();

        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        System.out.println("After 1 sec...");
                        convertTime();
                        if (totalSec <= 0 || submit.isVisible()) {
                            timer.cancel();
                            countdown.setText("00:00:00");
                            // saveing data to database
                            Notifications.create()
                                    .title("Error")
                                    .text("TIme Up")
                                    .position(Pos.BOTTOM_RIGHT)
                                    .showError();
                            submit.setVisible(true);
                            op1.setVisible(false);
                            op2.setVisible(false);
                            op3.setVisible(false);
                            op4.setVisible(false);


                        }
                    }
                });
            }
        };

        timer.schedule(timerTask, 0, 1000);
    }


    private void start() {

        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydatabase", "root", "@Root123");
            Statement stmt = connection.createStatement();
            String sql = "Select * from questions";
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {

                questions.add(rs.getString(1));
                op1s.add(rs.getString(2));
                op2s.add(rs.getString(3));
                op3s.add(rs.getString(4));
                op4s.add(rs.getString(5));
                answers.add(rs.getString(6));


            }

        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }

        submit.setVisible(false);
        question.setText(questions.get(Y));
        op1.setText(op1s.get(Y));
        op2.setText(op2s.get(Y));
        op3.setText(op3s.get(Y));
        op4.setText(op4s.get(Y));

        cont.setVisible(false);
    }


    public void gotonext(ActionEvent actionEvent) {
        res.setDisable(true);


        repeat();


    }

    private void repeat() {

        question.setText(questions.get(Y + 1));

        op1.setText(op1s.get(Y + 1));
        op2.setText(op2s.get(Y + 1));
        op3.setText(op3s.get(Y + 1));
        op4.setText(op4s.get(Y + 1));
        Y = Y + 1;


        cont.setVisible(false);
        op1.setVisible(true);
        op2.setVisible(true);
        op3.setVisible(true);
        op4.setVisible(true);
        setReset(op1);
        setReset(op2);
        setReset(op3);
        setReset(op4);

    }

    private void setReset(Button x) {
        x.setStyle("-fx-background-color:  #00003f; ");
        x.setTextFill(Color.WHITE);
    }

    public void finish(ActionEvent actionEvent) {
        Notifications.create()
                .darkStyle()
                .position(Pos.CENTER)
                .hideAfter(Duration.millis(2000))
                .text(" Complete your Quiz")
                .title("Finish").show();

       

        quiztab.setDisable(true);
        res.setDisable(false);


    }

    public void home(ActionEvent actionEvent) throws IOException {
        String finalResult = String.valueOf(Result1);
        System.out.println(finalResult);
        TextInputDialog td = new TextInputDialog("enter any text");

        // setHeaderText
        td.setHeaderText("enter your user name");
        td.setResizable(false);
        td.setTitle("Confirm Your acess");
        Stage stage = (Stage) td.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(this.getClass().getResource("icon1.png").toString()));


        td.showAndWait();


        String user = td.getEditor().getText();


                    System.out.println(user);
                    Connection conn = null;
                     Statement stmt = null;
                    try {
                        try {
                            Class.forName("com.mysql.cj.jdbc.Driver");
                        } catch (Exception e) {
                            System.out.println(e);
                        }
                        conn = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/mydatabase", "root", "@Root123");
                        System.out.println("Connection is created successfully:");
                        stmt = (Statement) conn.createStatement();
                        String query1 = "INSERT INTO results values('" + user + "','" + finalResult + "')";
                        stmt.executeUpdate(query1);

                        System.out.println("Record is inserted in the table successfully..................");
                    } catch (Exception excep) {
                        excep.printStackTrace();
                    } finally {
                        try {
                            if (stmt != null)
                                conn.close();
                        } catch (SQLException se) {
                        }
                        try {
                            if (conn != null)
                                conn.close();
                        } catch (SQLException se) {
                            se.printStackTrace();
                        }
                    }
                    System.out.println("Please check it in the MySQL Table......... ……..");

                    Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("hello-view.fxml")));
                    stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
                    Scene scene = new Scene(root);
                    stage.setScene(scene);
                    stage.setTitle("HOME");
                    stage.setResizable(false);
                    stage.show();




        }
    }

