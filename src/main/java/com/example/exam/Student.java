package com.example.exam;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;

import java.io.IOException;
import java.sql.*;
import java.util.Objects;
import java.util.regex.Pattern;

public class Student {
    @FXML
    private TextField firstname;
    @FXML
    private TextField lname;
    @FXML
    private TextField email;
    @FXML
    private TextField mobile;

    @FXML
    private TextField regcode;
    @FXML
    private TextField uname;
    @FXML
    private PasswordField pass1;
    @FXML
    private PasswordField repass1;
    @FXML
    private TextField ulname;
    @FXML
    private  TextField pwdl1;
    @FXML
    private  CheckBox showpwd;
    @FXML
    private  PasswordField passl1;
    @FXML
    private TextField pwdl11;



    public void register(ActionEvent actionEvent) {
        String firstName = firstname.getText();
        String lastName = lname.getText();
        String emailId = email.getText();
        String userName = uname.getText();
        String mobileNumber = mobile.getText();
        int len = mobileNumber.length();
        String password = pass1.getText();
        int len_p = password.length();
        String msg = "" + firstName;
        msg += " \n";

        boolean check = EmailValidation.isValid(email.getText());

        if (firstname.getText().equals("") || lname.getText().equals("") || email.getText().equals("") || uname.getText().equals("")){
            Notifications.create()
                    .darkStyle()
                    .position(Pos.CENTER)
                    .hideAfter(Duration.millis(2000))
                    .text(" Complete the Registration Form")
                    .title("IMPORTANT").showError();

        } else if (!check) {

            Notifications.create()
                    .darkStyle()
                    .position(Pos.CENTER)
                    .hideAfter(Duration.millis(2000))
                    .text(" Email is not Valid")
                    .title("IMPORTANT").showError();
            email.setText("");
        }else if (len != 10) {

            Notifications.create()
                    .darkStyle()
                    .position(Pos.CENTER)
                    .hideAfter(Duration.millis(2000))
                    .text(" Enter a valid mobile number")
                    .title("IMPORTANT").showError();

        }else if (len_p < 6) {
            Notifications.create()
                    .darkStyle()
                    .position(Pos.CENTER)
                    .hideAfter(Duration.millis(2000))
                    .text(" Password requires minimum 6 characters")
                    .title("IMPORTANT").showError();
        }else if(!pass1.getText().equals(repass1.getText())) {

            Notifications.create()
                    .darkStyle()
                    .position(Pos.CENTER)
                    .hideAfter(Duration.millis(2000))
                    .text(" password did not match")
                    .title("IMPORTANT").showError();
            pass1.setText("");
            repass1.setText("");
        }else {
            try {
                Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydatabase", "root", "@Root123");

                String query = "INSERT INTO account values('" + firstName + "','" + lastName + "','" + userName + "','" +
                        password + "','" +mobileNumber + "','" + emailId  + "')";

                Statement sta = connection.createStatement();
                int x = sta.executeUpdate(query);
                    Notifications.create()
                            .darkStyle()
                            .position(Pos.CENTER)
                            .hideAfter(Duration.millis(2000))
                            .text( "Welcome, " + msg + "Your account is sucessfully created")
                            .title("IMPORTANT").show();
                    firstname.setText("");
                    lname.setText("");
                    email.setText("");
                    uname.setText("");
                    mobile.setText("");
                    pass1.setText("");
                    repass1.setText("");


                connection.close();
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
    }

    public void showpwd(ActionEvent actionEvent) {
        String value = null;
        if (showpwd.isSelected()){
            pwdl11.setText(passl1.getText());
            pwdl11.setVisible(true);
            passl1.setVisible(false);

        }else{
            passl1.setText(pwdl11.getText());
            pwdl11.setVisible(false);
            passl1.setVisible(true);
        }
    }


    public static class EmailValidation {

        public static boolean isValid(String email){
            String emailFormate ="^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^-]+(?:\\."
                    + "[a-zA-Z0-9_!#$%&'*+/=?`{|}~^-]+)*@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$";

            Pattern p = Pattern.compile(emailFormate);
            if(email == null){
                return false;
            }
            return p.matcher(email).matches();
        }

    }


    public void reset(ActionEvent actionEvent) {
        firstname.setText("");
        lname.setText("");
        email.setText("");
        mobile.setText("");
        uname.clear();
        pass1.clear();
        repass1.clear();
        regcode.clear();



    }

    public void login(ActionEvent actionEvent) {
        System.out.println(pass1.getText());
        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydatabase", "root", "@Root123");
            Statement stmt = connection.createStatement();
            String sql="Select * from account where user_name='"+ulname.getText()+"'and password='"+passl1.getText()+"'";
            ResultSet rs=stmt.executeQuery(sql);
            if(rs.next()) {
                Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("studentDashboard.fxml")));
                Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.setTitle("STUDENT DASHBOARD");
                stage.setResizable(false);
                stage.show();

            } else {
                Notifications.create()
                        .darkStyle()
                        .position(Pos.CENTER)
                        .hideAfter(Duration.millis(2000))
                        .text( "Incorrect Username or Password")
                        .title("IMPORTANT").showError();
            }} catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }



    public void adminsignup(ActionEvent actionEvent) throws IOException {
        Parent root = FXMLLoader.load((getClass().getResource("admin.fxml")));
        Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("ADMIN LOGIN");
        stage.setResizable(false);

        stage.show();
    }

}
