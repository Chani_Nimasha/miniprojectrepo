package com.example.exam;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;

import java.io.IOException;
import java.sql.*;
import java.util.Objects;
import java.util.regex.Pattern;

public class Admin {
   @FXML
    private TextField firstname1;
 @FXML
 private TextField lname1;
 @FXML
 private TextField email1;
 @FXML
 private TextField mobile1;

 @FXML
 private TextField regcode;
 @FXML
 private TextField uname1;
 @FXML
 private PasswordField pwd1;
 @FXML
 private PasswordField Rpwd1;
 @FXML
 private TextField ulname1;
 @FXML
 private PasswordField pwdl2;

 @FXML
 private TextField pwdlett;



 @FXML
 private CheckBox showpwd1;



    public void register1(ActionEvent actionEvent) {

        String firstName = firstname1.getText();
        String lastName = lname1.getText();
        String emailId = email1.getText();
        String userName = uname1.getText();
        String mobileNumber = mobile1.getText();
        int len = mobileNumber.length();
        String password = pwd1.getText();
        int len_p = password.length();
        String msg = "" + firstName;
        msg += " \n";

        boolean check = EmailValidation1.isValid1(email1.getText());

        if (firstname1.getText().equals("") || lname1.getText().equals("") || email1.getText().equals("") || uname1.getText().equals("")){
            Notifications.create()
                    .darkStyle()
                    .position(Pos.CENTER)
                    .hideAfter(Duration.millis(2000))
                    .text(" Complete the Registration Form")
                    .title("IMPORTANT").showError();

        } else if (!check) {

            Notifications.create()
                    .darkStyle()
                    .position(Pos.CENTER)
                    .hideAfter(Duration.millis(2000))
                    .text(" Email is not Valid")
                    .title("IMPORTANT").showError();
            email1.setText("");
        }else if (len != 10) {

            Notifications.create()
                    .darkStyle()
                    .position(Pos.CENTER)
                    .hideAfter(Duration.millis(2000))
                    .text(" Enter a valid mobile number")
                    .title("IMPORTANT").showError();

        }else if (len_p < 6) {
            Notifications.create()
                    .darkStyle()
                    .position(Pos.CENTER)
                    .hideAfter(Duration.millis(2000))
                    .text(" Password requires minimum 6 characters")
                    .title("IMPORTANT").showError();
        }else if(!pwd1.getText().equals(Rpwd1.getText())) {

            Notifications.create()
                    .darkStyle()
                    .position(Pos.CENTER)
                    .hideAfter(Duration.millis(2000))
                    .text(" password did not match")
                    .title("IMPORTANT").showError();
            pwd1.setText("");
            Rpwd1.setText("");
        }else if(!regcode.getText().equals("123")){

            Notifications.create()
                    .darkStyle()
                    .position(Pos.CENTER)
                    .hideAfter(Duration.millis(2000))
                    .text(" Enter Valid code to Register as Admin")
                    .title("IMPORTANT").showError();
            regcode.clear();

        }



        else {
            try {
                Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydatabase", "root", "@Root123");

                String query = "INSERT INTO admin_account values('" + firstName + "','" + lastName + "','" + userName + "','" +
                        password + "','" + emailId + "','" + mobileNumber  + "')";

                Statement sta = connection.createStatement();
                int x = sta.executeUpdate(query);

                    Notifications.create()
                            .darkStyle()
                            .position(Pos.CENTER)
                            .hideAfter(Duration.millis(2000))
                            .text( "Welcome, " + msg + "Your account is sucessfully created")
                            .title("IMPORTANT").show();
                    firstname1.setText("");
                    lname1.setText("");
                    email1.setText("");
                    uname1.setText("");
                    mobile1.setText("");
                    pwd1.setText("");
                    Rpwd1.setText("");
                    regcode.clear();
                connection.close();
                } catch (SQLException throwables) {
                throwables.printStackTrace();
            }


        }



    }

    public static class EmailValidation1{

        public static boolean isValid1(String email){
            String emailFormate ="^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^-]+(?:\\."
                    + "[a-zA-Z0-9_!#$%&'*+/=?`{|}~^-]+)*@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$";

            Pattern p = Pattern.compile(emailFormate);
            if(email == null){
                return false;
            }
            return p.matcher(email).matches();
        }

    }


    public void reset1(ActionEvent actionEvent) {
        firstname1.setText("");
        lname1.setText("");
        email1.setText("");
        mobile1.setText("");
        uname1.clear();
        pwd1.clear();
        Rpwd1.clear();
        regcode.clear();
    }

    public void login1(ActionEvent actionEvent) {

        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydatabase", "root", "@Root123");
            Statement stmt = connection.createStatement();
            String sql="Select * from admin_account Where user_name='"+ulname1.getText()+"'and password='"+pwdl2.getText()+"'";
            ResultSet rs=stmt.executeQuery(sql);

            if(rs.next()) {
                Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("adminDashboard.fxml")));
                Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.setTitle("ADMIN DASHBOARD");
                stage.setResizable(false);
                stage.show();
            }
            else{
                Notifications.create()
                        .darkStyle()
                        .position(Pos.CENTER)
                        .hideAfter(Duration.millis(2000))
                        .text("Incorrect Username Or Password")
                        .title("IMPORTANT").showError();
            }

        } catch (SQLException | IOException sqlException) {
            sqlException.printStackTrace();
        }
    }


    public void usersignup(ActionEvent actionEvent) throws IOException {
        Parent root = FXMLLoader.load((getClass().getResource("student.fxml")));
        Stage stage = (Stage) ((Node)actionEvent.getSource()).getScene().getWindow();
        Scene  scene =new Scene(root);
        stage.setScene(scene);
        stage.setTitle("STUDENT LOGIN");
        stage.setResizable(false);

        stage.show();
    }

    public void show1(ActionEvent actionEvent) {
        if (showpwd1.isSelected()){
            pwdlett.setText(pwdl2.getText());
            pwdlett.setVisible(true);
            pwdl2.setVisible(false);
        }else{
            pwdlett.setText(pwdl2.getText());
            pwdlett.setVisible(false);
            pwdl2.setVisible(true);
        }
}}
