package com.example.exam;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;

import java.io.IOException;
import java.sql.*;
import java.util.*;

public class AdminDashboard  {


    public TextArea question;
    public TextField opA;
    public TextField opB;
    public TextField opC;
    public TextField opD;
    public CheckBox check1;
    public CheckBox check2;
    public CheckBox check3;
    public CheckBox check4;
    public String answer;
    public TableColumn fname =new TableColumn<>();
    public TableColumn lname=new TableColumn<>();
    public TableColumn uname=new TableColumn<>();
    public TableColumn  email=new TableColumn<>();
    public TableColumn  mobile=new TableColumn<>();
    public TableColumn result=new TableColumn<>();
    public TableView<studentInfo> table = new TableView(  );
    public TextField qn;

    public void main(String[] args) throws IOException {

    }

    public void add(ActionEvent actionEvent) {

        boolean valid = validation();
        if(valid){
            addQuestion();
        }
}

    private void addQuestion() {
        boolean validation =false;

        String que = question.getText();
        String op1 = opA.getText();
        String op2 = opB.getText();
        String op3 = opC.getText();
        String op4 = opD.getText();



        if (check1.isSelected()) {
            answer = opA.getText();
            check2.setDisable(true);
            check3.setDisable(true);
            check4.setDisable(true);
        }
        if (check2.isSelected()) {
            answer = opB.getText();
            check1.setDisable(true);
            check3.setDisable(true);
            check4.setDisable(true);
        }
        if (check3.isSelected()) {
            answer = opC.getText();
            check2.setDisable(true);
            check1.setDisable(true);
            check4.setDisable(true);
        }
        if (check4.isSelected()) {
            answer = opD.getText();
            check2.setDisable(true);
            check3.setDisable(true);
            check1.setDisable(true);
        }
        if ( !check1.isSelected() && !check2.isSelected() && !check3.isSelected() && !check4.isSelected() || check1.isSelected() && check2.isSelected()|| check1.isSelected() && check3.isSelected() ||check1.isSelected() && check4.isSelected() || check2.isSelected() && check3.isSelected() || check2.isSelected() && check4.isSelected() || check3.isSelected() && check4.isSelected() ){

            Notifications.create()
                    .darkStyle()
                    .position(Pos.CENTER)
                    .hideAfter(Duration.millis(2000))
                    .text(" SELECT AN ANSWER")
                    .title("IMPORTANT").showError();
            check2.setDisable(false);
            check3.setDisable(false);
            check1.setDisable(false);
            check4.setDisable(false);

        }
        if(qn.getText() !=null){
            try {
                Class.forName("com.mysql.cj.jdbc.Driver");
                Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydatabase", "root", "@Root123");
                Statement stmt = connection.createStatement();
                String sql="Select * from questions WHERE `question number`=" +qn.getText() + "";
                ResultSet rs=stmt.executeQuery(sql);
                if (rs.next()) {

                    System.out.println(rs.getString(7));


                          Notifications.create()
                            .darkStyle()
                            .position(Pos.CENTER)
                            .hideAfter(Duration.millis(2000))
                            .text(" QUESTION NUMBER ALREADY EXSIST")
                            .title("IMPORTANT").showError();
                             qn.clear();




                }
                else{
                    Notifications.create()
                            .darkStyle()
                            .position(Pos.CENTER)
                            .hideAfter(Duration.millis(2000))
                            .text(" UNIQUE QUESTION NUMBER")
                            .title("IMPORTANT").show();
                    Notifications.create()
                            .darkStyle()
                            .position(Pos.CENTER)
                            .hideAfter(Duration.millis(2000))
                            .text(" WELL DONE")
                            .title("IMPORTANT").show();


                    Connection conn = null;
                        stmt = null;
                    try {
                        try {
                            Class.forName("com.mysql.cj.jdbc.Driver");
                        } catch (Exception e) {
                            System.out.println(e);
                        }
                        conn = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/mydatabase", "root", "@Root123");
                        System.out.println("Connection is created successfully:");
                        stmt = (Statement) conn.createStatement();
                        String query1 = "INSERT INTO questions values('" + que + "','" + op1 + "','" + op2 + "','" +
                                op3 + "','" + op4 + "','" + answer + "','" + qn.getText() +"')";
                        stmt.executeUpdate(query1);

                        System.out.println("Record is inserted in the table successfully..................");
                    } catch (Exception excep) {
                        excep.printStackTrace();
                    } finally {
                        try {
                            if (stmt != null)
                                conn.close();
                        } catch (SQLException se) {}
                        try {
                            if (conn != null)
                                conn.close();
                        } catch (SQLException se) {
                            se.printStackTrace();
                        }
                    }
                    System.out.println("Please check it in the MySQL Table......... ……..");

                    check2.setDisable(false);
                    check3.setDisable(false);
                    check1.setDisable(false);
                    check4.setDisable(false);
                    check2.setSelected(false);
                    check3.setSelected(false);
                    check1.setSelected(false);
                    check4.setSelected(false);
                    question.clear();
                    opA.clear();
                    opB.clear();
                    opC.clear();
                    opD.clear();
                    qn.clear();


                }

            } catch (SQLException | ClassNotFoundException sqlException) {
                sqlException.printStackTrace();
            }






        }





    }

    private boolean validation() {

        if (question.getText().isEmpty() || qn.getText().isEmpty()|| opA.getText().isEmpty() || opB.getText().isEmpty() || opC.getText().isEmpty() || opD.getText().isEmpty()) {
            Notifications.create()
                    .darkStyle()
                    .position(Pos.CENTER)
                    .hideAfter(Duration.millis(2000))
                    .text(" ALL FEILDS ARE REQUIRED")
                    .title("IMPORTANT").showError();
            return false;
        }





        else {
            Notifications.create()
                    .darkStyle()
                    .position(Pos.CENTER)
                    .hideAfter(Duration.millis(2000))
                    .text(" Well TextField  are filled  Now Select Correct Answer")
                    .title("IMPORTANT").show();


        }

        return true;

    }
    public void students(Event event) {


        fname.setCellValueFactory(new PropertyValueFactory<studentInfo, String>("fnCol"));
        lname.setCellValueFactory(new PropertyValueFactory<studentInfo, String>("lnCol"));
        uname.setCellValueFactory(new PropertyValueFactory<studentInfo, String>("userCol"));
        email.setCellValueFactory(new PropertyValueFactory<studentInfo, String>("emailCol"));
        mobile.setCellValueFactory(new PropertyValueFactory<studentInfo, String>("mobCol"));
        result.setCellValueFactory(new PropertyValueFactory<studentInfo, String>("resCol"));
        table.getItems().setAll(getAllstudentInfo());
    }
    private List<studentInfo> getAllstudentInfo() {
        List<studentInfo> ll = new LinkedList<>();
        String username =null;
        String frname=null;
        String lname=null;
        String mobile=null;
        String email=null;
        String user=null;
        String result1=null;
        String result = null;

        try {


            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydatabase", "root", "@Root123");
            Statement stmt = connection.createStatement();

            String sql = "SELECT  results.result,account.first_name,account.last_name,account.user_name,account.mobile_number,account.email_id from  account INNER JOIN results ON results.userName = account.user_name ";
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                frname = rs.getString("first_name");
                lname = rs.getString("last_name");
                username = rs.getString("user_name");
                mobile = rs.getString("mobile_number");
                email = rs.getString("email_id");
                result=rs.getString("result");

                result1 = null;

                   result1 = result;
                   System.out.println(result1);

                if (result == null){
                    result1 ="Pending";
                }



                ll.add(new studentInfo(frname,lname ,username,email, mobile, result1));





            }

        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }


        return ll;
    }

    public void delete(ActionEvent actionEvent) {

        TextInputDialog td = new TextInputDialog("enter any text");

        // setHeaderText
        td.setHeaderText("Enter the Question Number");
        td.setResizable(false);
        td.setTitle("DELETE");
        Stage stage = (Stage) td.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(this.getClass().getResource("icon1.png").toString()));



        td.showAndWait();

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            // establish connection
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydatabase", "root", "@Root123");
            Statement statement = con.createStatement();
            statement.executeUpdate("DELETE FROM questions WHERE `question number`=" +td.getEditor().getText() + "");

            Notifications.create()
                    .darkStyle()
                    .position(Pos.CENTER)
                    .hideAfter(Duration.millis(2000))
                    .text(" QUESTION IS DELETED SUCCESSFULLY")
                    .title("IMPORTANT").show();

            statement.close();
            con.close();
            //Calling Referesh() method
        } catch (SQLException | ClassNotFoundException e) {
            Notifications.create()
                    .darkStyle()
                    .position(Pos.CENTER)
                    .hideAfter(Duration.millis(2000))
                    .text(" INVALID QUESTION NUMBER")
                    .title("IMPORTANT").showError();

        }

    }
}
